# Zero to Full DevOps Stack

This project outlines details the project plan with documentation, references, and code snippets to go from zero (no infrastructure) to a full DevOps stack using only open source tech the whole way through.